import os
from random import shuffle

import numpy as np
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.decomposition import PCA


# Compute distances among first X words (depending on your machine)
def find_closest_words(max_words, embeddings_index, embeddings_size):
    mat = pairwise_distances(list(embeddings_index.values())[:max_words])

    # Replace self distances from 0 to inf (to use argmin)
    np.fill_diagonal(mat, np.inf)

    # Compute the most similar word for every word
    min_0 = np.argmin(mat, axis=0)

    # Save the pairs to a file
    f_out = open('similarity_pairs_dim' + embeddings_size + '_first' + str(max_words) + '.txt', 'w')
    for i, item in enumerate(list(embeddings_index.keys())[:max_words]):
        f_out.write(str(item) + ' ' + str(list(embeddings_index.keys())[min_0[i]]) + '\n')


def decrease_dimensions(new_dimension, embeddings_full):
    embeddings_index_small = {}

    pca = PCA(n_components=new_dimension)
    values_pca = pca.fit_transform(list(embeddings_full.values()))

    for i, key in enumerate(list(embeddings_full.keys())):
        embeddings_index_small[key] = values_pca[i]

    return embeddings_index_small


def load_vectors(embeddings_size):
    # Create a dictionary/map to store the word embeddings
    embeddings_map = {}

    # Load pre-computed word embeddings
    # These can be dowloaded from https://nlp.stanford.edu/projects/glove/
    # e.g., wget http://nlp.stanford.edu/data/glove.6B.zip
    f = open(os.path.join('.', 'glove.6B.' + embeddings_size + 'd.txt'), encoding="utf-8")

    print("Loading file...")
    # Process file and load into structure
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_map[word] = coefs
    f.close()

    return embeddings_map


def print_analogy(embeddings_index, association, new_word):
    embedding_analogy = association + embeddings_index[new_word]
    # Find distances with the rest of the words
    analogy_distances = np.empty(len(embeddings_index))
    for i, item in enumerate(list(embeddings_index.values())):
        analogy_distances[i] = pairwise_distances(embedding_analogy.reshape(1, -1), item.reshape(1, -1))
    # Print top 10 results
    print([list(embeddings_index.keys())[i] for i in analogy_distances.argsort()[:10]])


def test_capitals(embeddings_size, embeddings_index):
    file = open(os.path.join('.', 'countries_capitals_world.txt'), encoding="utf-8")
    capitals = {}

    # association_mean = np.zeros(int(embeddings_size))
    association_mean = embeddings_index["madrid"] - embeddings_index["spain"]
    file_length = 0

    for line in file:
        values = line.split()
        country = values[0].lower()
        capital = values[1].lower()
        capitals[country] = capital

        # association_mean += embeddings_index[capital] - embeddings_index[country]

        file_length += 1

    file.close()

    # association_mean /= file_length
    correct = [0, 0, 0]

    for j, country in enumerate(list(capitals.keys())):
        embedding_analogy = association_mean + embeddings_index[country]
        # Find distances with the rest of the words
        analogy_distances = np.empty(len(embeddings_index))
        for i, item in enumerate(list(embeddings_index.values())):
            analogy_distances[i] = pairwise_distances(embedding_analogy.reshape(1, -1), item.reshape(1, -1))

        closest = analogy_distances.argsort()[:3]

        for i in range(3):
            if list(embeddings_index.keys())[closest[i]] == capitals[country]:
                correct[i] += 1
        print("PROGRESS:", j + 1, '/', file_length)

    print("RESULTS")

    for i, c in enumerate(correct):
        print(i + 1, ' closest: ', c)
    print("Rest: ", file_length - sum(correct))


def test_capitals_diff_sizes():
    print("50d:")
    test_capitals("50", load_vectors("50"))

    print("100d:")
    test_capitals("100", load_vectors("100"))

    print("200d:")
    test_capitals("200", load_vectors("200"))

    print("300d:")
    test_capitals("300", load_vectors("300"))


def print_minimized_vectors(embeddings_index):
    embeddings_index_pca = decrease_dimensions(2, embeddings_index)

    file = open(os.path.join('.', 'countries_capitals_world.txt'), encoding="utf-8")
    capitals = {}

    for line in file:
        values = line.split()
        country = values[0].lower()
        capital = values[1].lower()
        capitals[country] = capital
    file.close()
    shuffled_capitals = list(capitals.keys())
    shuffle(shuffled_capitals)
    for country in shuffled_capitals[:10]:
        print(country, ": ", embeddings_index_pca[country])
        capital = capitals[country]
        print(capital, ": ", embeddings_index_pca[capital])


test_capitals_diff_sizes()

embeddings_size = "300"
embeddings_index = load_vectors(embeddings_size)
print_minimized_vectors(embeddings_index)

print_analogy(embeddings_index, embeddings_index['warsaw'] - embeddings_index['poland'], "spain")
