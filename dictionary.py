import os
from random import shuffle
import io

import numpy as np
from gensim.models.translation_matrix import Space
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.decomposition import PCA

from gensim.models import translation_matrix, TranslationMatrix
from gensim.models import KeyedVectors


# Compute distances among first X words (depending on your machine)
def find_closest_words(max_words, embeddings_index, embeddings_size):
    mat = pairwise_distances(list(embeddings_index.values())[:max_words])

    # Replace self distances from 0 to inf (to use argmin)
    np.fill_diagonal(mat, np.inf)

    # Compute the most similar word for every word
    min_0 = np.argmin(mat, axis=0)

    # Save the pairs to a file
    f_out = open('similarity_pairs_dim' + embeddings_size + '_first' + str(max_words) + '.txt', 'w')
    for i, item in enumerate(list(embeddings_index.keys())[:max_words]):
        f_out.write(str(item) + ' ' + str(list(embeddings_index.keys())[min_0[i]]) + '\n')


def decrease_dimensions(new_dimension, embeddings_full):
    embeddings_index_small = {}

    pca = PCA(n_components=new_dimension)
    values_pca = pca.fit_transform(list(embeddings_full.values()))

    for i, key in enumerate(list(embeddings_full.keys())):
        embeddings_index_small[key] = values_pca[i]

    return embeddings_index_small


def load_vectors(language):
    fin = io.open(os.path.join('.', 'cc.' + language + '.300.vec'), 'r', encoding='utf-8', newline='\n', errors='ignore')
    n, d = map(int, fin.readline().split())
    data = {}
    for i, line in enumerate(fin):
        tokens = line.rstrip().split(' ')
        data[tokens[0]] = np.asarray(tokens[1:], dtype='float32')
        if i % (n / 100) == 0:
            print("loading", i/n*100, "%")
    return data


def print_analogy(embeddings_index, association, new_word):
    embedding_analogy = association + embeddings_index[new_word]
    # Find distances with the rest of the words
    analogy_distances = np.empty(len(embeddings_index))
    for i, item in enumerate(list(embeddings_index.values())):
        analogy_distances[i] = pairwise_distances(embedding_analogy.reshape(1, -1), item.reshape(1, -1))
    # Print top 10 results
    print([list(embeddings_index.keys())[i] for i in analogy_distances.argsort()[:10]])


def print_closest_word(embeddings_index, word_index):
    # Find distances with the rest of the words
    analogy_distances = np.empty(len(embeddings_index))
    for i, item in enumerate(list(embeddings_index.values())):
        analogy_distances[i] = pairwise_distances(word_index.reshape(1, -1), item.reshape(1, -1))
    # Print top 10 results
    print([list(embeddings_index.keys())[i] for i in analogy_distances.argsort()[:10]])


def print_minimized_vectors(embeddings1, embeddings2):
    decreased1 = decrease_dimensions(2, embeddings1)
    decreased2 = decrease_dimensions(2, embeddings2)

    print("mouse:", decreased1["mouse"])
    print("three:", decreased1["three"])
    print("four:", decreased1["four"])
    print("horse", decreased1["horse"])

    print("mysz:", decreased2["mysz"])
    print("trzy:", decreased2["trzy"])
    print("cztery:", decreased2["cztery"])
    print("koń:", decreased2["koń"])


def align_embeddings(embedding1, embedding2):
    train_file = open(os.path.join('.', 'en-pl.train.txt'), encoding="utf-8")
    word_pair = []
    for line in train_file:
        pair = line.strip().split()
        word_pair.append((pair[1], pair[0]))
    train_file.close()

    # create translation matrix object
    trans_matrix = translation_matrix.TranslationMatrix(embedding1, embedding2, word_pair)
    print("trans matrix created")
    trans_matrix.train(word_pair)

    try:
        trans_matrix.save("trans.txt")
    except:
        print("Saving file was unsuccessful")

    return trans_matrix


def delete_unknown_words(embeddings, filename, word_number):
    train_file = open(os.path.join('.', filename), encoding="utf-8")
    word_pair = [tuple(line.strip().split()) for line in train_file]
    train_file.close()

    file = open(filename, "w", encoding="utf-8")
    for pair in word_pair:
        for word in list(embeddings.keys()):
            if pair[word_number] == word:
                file.write(pair[0] + ' ' + pair[1] + '\n')
                break
    file.close()


def test_translated(trans_matrix):
    test_file = open(os.path.join('.', 'en-pl.test.txt'), encoding="utf-8")
    word_pairs = []
    for line in test_file:
        pair = line.strip().split()
        word_pairs.append((pair[1], pair[0]))
    word_dictionary = {}
    test_file.seek(0)
    for line in test_file:
        pair = line.strip().split()
        word_dictionary[pair[1]] = pair[0]
    test_file.close()

    word_pairs = word_pairs[600:800]
    source_word, target_word = zip(*word_pairs)
    translated_words = trans_matrix.translate(source_word, 5)

    in_one = in_three = in_five = total = 0

    for word, translation in translated_words.items():
        total += 1
        if word_dictionary[word] in translation:
            in_five += 1
        if word_dictionary[word] in translation[:3]:
            in_three +=1
        if word_dictionary[word] == translation[0]:
            in_one += 1

    print('In one: ', in_one/total)
    print('In three: ', in_three/total)
    print('In five: ', in_five/total)


def print_translated_minimized_other(trans_matrix):
    words = [("dog", "pies"), ("cat", "kot"), ("horse", "koń"), ("two", "dwa"), ("three", "trzy"), ("four", "cztery")]
    en_words, pl_words = zip(*words)

    target_space = Space.build(trans_matrix.target_lang_vec)

    translated_words = trans_matrix.translate(en_words, 1)

    pca = PCA(n_components=2)
    new_pl_words_vec = pca.fit_transform(target_space.mat)

    for word in translated_words:
        print(new_pl_words_vec[target_space.word2index[word]])


def print_translated_minimized(trans_matrix):
    source_space = Space.build(trans_matrix.source_lang_vec)
    translated_space = trans_matrix.apply_transmat(source_space)

    pca = PCA(n_components=2)

    target_space = Space.build(trans_matrix.target_lang_vec)
    mat = target_space.mat
    mat.append(translated_space.mat[translated_space.word2index["dog"]])
    mat.append(translated_space.mat[translated_space.word2index["cat"]])
    mat.append(translated_space.mat[translated_space.word2index["horse"]])
    mat.append(translated_space.mat[translated_space.word2index["two"]])
    mat.append(translated_space.mat[translated_space.word2index["three"]])
    mat.append(translated_space.mat[translated_space.word2index["four"]])

    new_pl_words_vec = pca.fit_transform(mat)

    print(new_pl_words_vec[-6:])


embeddings_en = load_vectors("en")
embeddings_pl = load_vectors("pl")

delete_unknown_words(embeddings_pl, 'en-pl.train.txt', 1)
delete_unknown_words(embeddings_pl, 'en-pl.test.txt', 1)
delete_unknown_words(embeddings_en, 'en-pl.test.txt', 0)

print_minimized_vectors(embeddings_en, embeddings_pl)

embedding_en = KeyedVectors.load_word2vec_format(os.path.join('.', 'cc.en.300.vec'))
print("File en loaded")

embedding_pl = KeyedVectors.load_word2vec_format(os.path.join('.',  'cc.pl.300.vec'))
print("file pl loaded")

translation_mat = align_embeddings(embedding_pl, embedding_en)


loaded_trans_model = TranslationMatrix.load("trans.txt")
print("loaded")

# test_translated(loaded_trans_model)
print_translated_minimized_other(loaded_trans_model)